//
//  BoomDelegate.swift
//  Base Battle
//
//  Copyright © 2019 fusionblender John Banks. All rights reserved.
//

import Foundation

protocol BoomDelegate{
    
    func boom(sender:Any?)
}
