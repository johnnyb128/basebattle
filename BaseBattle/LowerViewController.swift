//
//  LowerViewController.swift
//  Base Battle
//
//  Copyright © 2019 fusionblender John Banks. All rights reserved.
//

import UIKit
import AVFoundation

class LowerViewController: UIViewController {

    @IBOutlet weak var lowerFire: UIButton!
   
    var boomDelegate:BoomDelegate!
    var lowerPlayer: AVAudioPlayer?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let lowerSoundAsset:NSDataAsset = NSDataAsset.init(name: "laser_2")!
        
        do {
            lowerPlayer = try AVAudioPlayer(data: lowerSoundAsset.data, fileTypeHint: AVFileType.mp3.rawValue)
        } catch let error {
            print("Error setting up audio players: \(error)")
        }
    }
    

    /**
     Tells the delegate to fire the weapon sending tbe Lower VC as sender and
     passes that VC (self) onto the delegate for processing
     
     - Parameter
     - sender: The sending, embedded, UIViewController.  In this case, self.
     */
    @IBAction func triggerWeapon(_ sender: Any) {
        
        boomDelegate.boom(sender: self)
        
    }
   
}
