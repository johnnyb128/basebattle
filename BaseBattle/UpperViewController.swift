//
//  UpperViewController.swift
//  Base Battle
//
//  Copyright © 2019 fusionblender John Banks. All rights reserved.
//

import UIKit
import AVFoundation

class UpperViewController: UIViewController {

    @IBOutlet weak var upperFire: UIButton!
    
    var boomDelegate:BoomDelegate!
    var upperPlayer: AVAudioPlayer?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let upperSoundAsset:NSDataAsset = NSDataAsset.init(name: "laser_1")!
        
        do {
            upperPlayer = try AVAudioPlayer(data: upperSoundAsset.data, fileTypeHint: AVFileType.mp3.rawValue)
        } catch let error {
            print("Error setting up audio players: \(error)")
        }
        
    }
    
    /**
     Tells the delegate to fire the weapon sending tbe Upper VC as sender and
     passes that VC (self) onto the delegate for processing
     
     - Parameter
     - sender: The sending, embedded, UIViewController.  In this case, self.
     */
    @IBAction func triggerWeapon(_ sender: Any) {
        boomDelegate.boom(sender: self)
        
    }

}
