//
//  WinScreenViewController.swift
//  BaseBattle
//
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class WinScreenViewController: UIViewController {
    
    @IBOutlet weak var resetScores: UIButton!
    @IBOutlet weak var bottomWinCountLabel: UILabel!
    @IBOutlet weak var topWinCountLabel: UILabel!
    @IBOutlet weak var playAgainButton: UIButton!
    @IBOutlet weak var winnerLabel: UILabel!
    
    let SCORES_KEY = "scores_key"
    
    var winner:Winner!
    var winnerColor:UIColor!
    
    // Closure as a demo to pass info back
    var passBackClosure: ((Winner)->())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set color, text, and transform based on the winner set from main game screen
        self.view.backgroundColor = winnerColor
        winnerLabel.text = (winner == .orange) ? "Orange Wins!!!" : "Blue Wins!!!"
        winnerLabel.transform = (winner == .orange) ? CGAffineTransform.init(rotationAngle:0) : CGAffineTransform.init(rotationAngle:.pi)
        
        topWinCountLabel.transform = CGAffineTransform.init(rotationAngle: .pi)
        
        // Demo of UserDefaults and how to store/pull from stored values there
        // if the dictionary exists for the given key pull from it
        if var currentScore = UserDefaults.standard.dictionary(forKey: SCORES_KEY){
           
            // Set the current text and stored values depending on winner set from main game screen
            if let upper = currentScore["orange"] as? Int{
                topWinCountLabel.text = (winner == .orange) ?  String(upper + 1) : String(upper)
                currentScore["orange"] = (winner == .orange) ? upper + 1 : upper
            }
       
            if let lower = currentScore["blue"] as? Int{
                bottomWinCountLabel.text = (winner == .blue) ?  String(lower + 1) : String(lower)
                currentScore["blue"] = (winner == .blue) ? lower + 1 : lower
            }
       
            
            UserDefaults.standard.set(currentScore, forKey: SCORES_KEY)
            
        }else{ //otherwise create a new dictionary, set the score based on the winner, and save it.
            
            var scores:[String:Int] = (winner == .orange) ? [Winner.orange.rawValue:1, Winner.blue.rawValue:0] : [Winner.orange.rawValue:0, Winner.blue.rawValue: 1]
            UserDefaults.standard.set(scores, forKey: SCORES_KEY)

            topWinCountLabel.text = String(scores[Winner.orange.rawValue]!)
            bottomWinCountLabel.text = String(scores[Winner.blue.rawValue]!)
            
        }

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        // Disable the buttons so the animation plays in full before user can play again or reset scores.
        playAgainButton.isUserInteractionEnabled = false
        resetScores.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.5, animations: {
             self.winnerLabel.transform = CGAffineTransform.init(scaleX: 1.5, y: 1.5)
        }, completion: {_ in
            UIView.animate(withDuration: 0.5, animations: {
                self.winnerLabel.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
            })
            
        });
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(3000)) {
            
            UIView.animate(withDuration: 0.5, animations: {
                
                // Flip the scores so other player can see
                self.topWinCountLabel.transform = CGAffineTransform.init(rotationAngle: 3.14159 - .pi)
                self.bottomWinCountLabel.transform = CGAffineTransform.init(rotationAngle: .pi)
                
                // Flip the winner text so other player can see
                self.winnerLabel.transform = (self.winner == .orange) ? CGAffineTransform.init(rotationAngle:.pi) :  CGAffineTransform.init(rotationAngle:3.14159 - .pi)
            }, completion: { (Bool) in
                
                UIView.animate(withDuration: 0.5, delay: 1.5, options: [], animations: {
                    
                    // reset all the positions
                    self.topWinCountLabel.transform = CGAffineTransform.init(rotationAngle: .pi)
                    self.bottomWinCountLabel.transform = CGAffineTransform.init(rotationAngle: 3.14159 - .pi)
                    self.winnerLabel.transform = (self.winner == .orange) ? CGAffineTransform.init(rotationAngle:.pi)  : CGAffineTransform.init(rotationAngle:3.14159 - .pi)
                }, completion: { (Bool) in
                    // allow user to reset scores or play again
                    self.playAgainButton.isUserInteractionEnabled = true
                    self.resetScores.isUserInteractionEnabled = true
                })
            })
            
        }
        
    }

    /**
     Connected to reset scores button on WinScreenVC to reset the score for top and bottom player
     
     - Parameter
     - sender: The sending object, in this case, the resetScores button
     */
    @IBAction func resetScores(_ sender: Any) {
    
        // completely remove dictionary if stored and set text to 0
        UserDefaults.standard.removeObject(forKey: SCORES_KEY)
        self.topWinCountLabel.text = "0"
        self.bottomWinCountLabel.text = "0"
    }
    
    /**
     Connected to play again button on WinScreenVC return to the game screen.
     Passes back to main VC the Winner for animating their button as loser
     
     - Parameter
     - sender: The sending object, in this case, the playAgain button
     */
    @IBAction func returnToMainScreen(_ sender: Any) {
        
        self.passBackClosure?(winner)
        
    }

}
