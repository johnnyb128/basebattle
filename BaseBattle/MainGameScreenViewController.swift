//
//  ViewController.swift
//  Base Battle
//
//  Copyright © 2019 fusionblender John Banks. All rights reserved.
//

import UIKit
import AVFoundation


enum Winner:String{
    case orange, blue
}

class MainGameScreenViewController: UIViewController {

    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var upperContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lowerContainerHeightConstraint: NSLayoutConstraint!
    weak var lowerVC:LowerViewController!
    weak var upperVC:UpperViewController!
    let HIT_STRENGTH:CGFloat = 5
    
    var lowerShootCount = 0
    var upperShootCount = 0
    
    var countdownTimer:Timer!
    var countdownVal = 3
    var lastLoser:UIButton? = nil
    
  
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        do{
            // Get the AVAudioSession
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
        } catch let error {
            print("Error setting up audio players: \(error)")
        }
        
        setBaseHeight()
        self.countdownLabel.textColor = UIColor.white.withAlphaComponent(0.8)
        self.overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.countdownLabel.text = ""
       
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        shakeLoser()
       
        // Start the timer to display countdown to end user
        countdownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(animateCountdown), userInfo: nil, repeats: true)
    }
    
    
    @IBAction func unwindFromWinScreen(_ sender:UIStoryboardSegue){
        
        // Reset the base heights back to original
        setBaseHeight()
        overlayView.alpha = 1.0
        overlayView.isHidden = false
        
        // Reset the shoot counts - someone won while shots were still in route.
        // without resetting this the calculation for where to stop the shot
        // would be incorrect
        lowerShootCount = 0
        upperShootCount = 0
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        // Setup the channels of communication between Child VCs and Parent via BoomDelegate
        if segue.identifier == "uppercontainer"{
            upperVC = segue.destination as? UpperViewController
            upperVC.boomDelegate = self
        }else if segue.identifier == "lowercontainer"{
            lowerVC = segue.destination as? LowerViewController
            lowerVC.boomDelegate = self
        }else if segue.identifier == "winScreenSegue"{ //
            
            // Set the WinScreenVC callback closure
            // This is a demo of passing data backwards via callback
            if let winVC = segue.destination as? WinScreenViewController{
                
                winVC.passBackClosure = {winner in
                    if let winner_return_value = winner as? Winner{
                        // point the button reference to the losing side
                        self.lastLoser = winner_return_value == .orange ? self.lowerVC.lowerFire : self.upperVC.upperFire
                        
                    }

                }
                
                // Set the winner and color in the newly created WinScreenVC
                if let _ = sender as? UpperViewController{
                    winVC.winner = .orange
                    winVC.winnerColor = self.upperVC.view.backgroundColor!
                }else if let _ = sender as? LowerViewController{
                    winVC.winner = .blue
                    winVC.winnerColor = self.lowerVC.view.backgroundColor!
                }
                
                
            }
        }
    }

    /**
     Takes in a player's embedded UIViewController and calculates a random point to shoot towards
     based on that ViewControllers current frame
     
     - Parameter
        - sender: The sending, embedded, UIViewController.  Either top or bottom player's side

     - Returns:
        - CGPoint indicating the hit point on the opposite side's UIViewController
     */
    private func calculateHitPoint(sender: Any?) -> CGPoint{
        
        var hitPoint:CGPoint = CGPoint.init()
        
        if let _ = sender as? UpperViewController{
            
            let lowerRect = lowerVC.view.superview?.convert(lowerVC.view.frame, to: nil)
            hitPoint.y = lowerRect!.minY + (HIT_STRENGTH * CGFloat(upperShootCount))
            hitPoint.x = CGFloat(arc4random_uniform(UInt32(lowerRect!.maxX - lowerRect!.minX))) + lowerRect!.minX
           
            
        }else if let _ = sender as? LowerViewController{
            
            let upperRect = upperVC.view.superview?.convert(upperVC.view.frame, to: nil)
            hitPoint.y = upperRect!.maxY - (HIT_STRENGTH * CGFloat(lowerShootCount))
            hitPoint.x = CGFloat(arc4random_uniform(UInt32(upperRect!.maxX - upperRect!.minX))) + upperRect!.minX
            
            
        }
        
        return hitPoint
    }
    
    /**
     Called from BoomDelegate to render a shot on the screen
     
     - Parameter
     - sender: The sending, embedded, UIViewController either top or bottom player's UIViewController.
     */
    private func shoot(sender: Any?){
        
   
        let pathLayer = CAShapeLayer()
        let path = UIBezierPath()
        
        // Generic values to simplify the code.
        // Reused for upper or lower VC shot calculation and color.
        var center:CGPoint?
        var lineColor:CGColor?
        var heightConstraint:NSLayoutConstraint!
      
        // Cast the sender to determine if method call came from top or bottom VC.
        if let uvc = sender as? UpperViewController{
            
            //Convert the center point of the fire button inside the Child VC
            //into the coordinates of the Parent VC
            center = uvc.view?.convert(uvc.upperFire.center, to: self.view)
            lineColor = UIColor.red.cgColor
            
            // Increment the shoot count of the top VC.
            // Used to calculate the predicted Y value of the opponent's base,
            // based on the number of shots inbound..
            upperShootCount += 1
            
            // Set the generic heightConstraint to the opponent's VC height
            heightConstraint = lowerContainerHeightConstraint
            
            // Cast the sender to determine if method call came from top or bottom VC.
        }else if let lvc = sender as? LowerViewController{
            
            // See comments above, flipped for opposite VC
            center = lvc.view?.convert(lvc.lowerFire.center, to: self.view)
            lineColor = UIColor.blue.cgColor
            
            lowerShootCount += 1
        
            heightConstraint = upperContainerHeightConstraint
            
        }
        
        CATransaction.begin()
        
        // Completion block used to remove the animated shot CALayer from the view's layer
        // and determine if a player has won the game based on this shot.
        CATransaction.setCompletionBlock({
            
            pathLayer.removeFromSuperlayer()
            
            // Prevent constraint from going negative and throwing output errors
            if heightConstraint.constant - self.HIT_STRENGTH >= 0{
                
                heightConstraint.constant = heightConstraint.constant - self.HIT_STRENGTH
                
                if sender is UpperViewController{
                    
                    // Decrement the pending shoot count since that shot is complete -
                    // necessary for calculating the correct CGPoint on opponents Base
                    self.upperShootCount -= 1
                    
                    // If the heightConstraint of opponent's base is 0, show win screen
                    if heightConstraint.constant == 0{
                        self.performSegue(withIdentifier: "winScreenSegue", sender: self.upperVC)
                    }
                    
                }else{
                    
                    // Decrement the pending shoot count since that shot is complete -
                    // necessary for calculating the correct CGPoint on opponents Base
                    self.lowerShootCount -= 1
                    
                    // If the heightConstraint of opponent's base is 0, show win screen
                    if heightConstraint.constant == 0{
                        self.performSegue(withIdentifier: "winScreenSegue", sender: self.lowerVC)
                    }
                }
                
            }
           
        })
        
        // position initial point of shot to render
        path.move(to: CGPoint(x: center!.x, y: center!.y))
        
        // addLine to the calculated CGPoint on opponent's base, returned from calculateHitPoint
        path.addLine(to: calculateHitPoint(sender: sender))
        
        
        //Add the shot CALayer to the superview's layer
        pathLayer.frame = view.bounds
        pathLayer.path = path.cgPath
        pathLayer.strokeColor = lineColor!
        pathLayer.fillColor = nil
        pathLayer.lineWidth = 2
        pathLayer.lineJoin = CAShapeLayerLineJoin.bevel
        view.layer.addSublayer(pathLayer)
        
        // Set animation parameters
        let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pathAnimation.duration = 2.0
        pathAnimation.fromValue = 0
        pathAnimation.toValue = 1
        pathLayer.add(pathAnimation, forKey: "strokeEnd")
        
        // DO IT!
        CATransaction.commit()

    }
    
    /**
     Called from viewDidAppear() on a recurring timer to render an animated countdown to the end user.
     Prevents user from firing by using an overlay on top of the game screen to prevent button touches
     & show countdown.
     */
    @objc private func animateCountdown(){
        
        // Start by shrinking the current countdown value
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            
            self.countdownLabel.transform = CGAffineTransform.init(scaleX: 0.0, y: 0.0)
            
        }) { _ in
            
            // Once size is 0, set to new number currently in class parameter countdownVal
            self.countdownLabel.text = String(self.countdownVal)
            
            // Enlarge the countdown value
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
                
                self.countdownLabel.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
                
            }){_ in
                
                // Decrement the countdown since we've done a round now.
                self.countdownVal -= 1
                
                // If reached 0 invalidate the timer so it doesn't trigger again
                if self.countdownVal == 0{
                    
                    self.countdownTimer.invalidate()
                    self.countdownTimer = nil
                    
                    //Animate the disappearance of the overlay
                    UIView.animate(withDuration: 0.5, animations: {
                        self.overlayView.alpha = 0.0
                    }, completion:{_ in
                        // hide the overlay and reset the countdown values.
                        self.overlayView.isHidden = true
                        self.countdownVal = 3
                        self.countdownLabel.text = ""
                    })
                }
            }
            
        }

    }
    
    /**
     Animate the losing side's fire button to shake
     */
    private func shakeLoser(){

        if self.lastLoser != nil{
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 40
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: lastLoser!.center.x - 20, y: lastLoser!.center.y ))
            animation.toValue = NSValue(cgPoint: CGPoint(x: lastLoser!.center.x + 20, y: lastLoser!.center.y ))
            
            lastLoser?.layer.add(animation, forKey: "position")
        }
    }
    
    /**
     Set the size of each player's "base" to an appropriate size for device type.
     */
    private func setBaseHeight(){
        
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_REG{
            lowerContainerHeightConstraint.constant = 275
            upperContainerHeightConstraint.constant = 275
        }else{
            lowerContainerHeightConstraint.constant = 350
            upperContainerHeightConstraint.constant = 350
        }
        
    }

}

//MARK:- BoomDelegate
extension MainGameScreenViewController:BoomDelegate{
    
    /**
     Protocol method used to track which player has fired the shot and
     subsequently be able to calculate where on the opponent's base/opposite VC
     to shoot
     
     - Parameter
     - sender: The sending, embedded, UIViewController either top or bottom player's UIViewController.
     */
    func boom(sender: Any?) {
        shoot(sender: sender)
        pewPew(sender: sender)
        
    }

}

//MARK:- Sounds
extension MainGameScreenViewController{
    
    /**
     Plays the respective sound based on the incoming players base/ViewController
     Player setup is in viewDidLoad()
     
     - Parameter
     - sender: The sending, embedded, UIViewController either top or bottom player's UIViewController.
     */
    func pewPew(sender:Any?){
        
        
        if let _ = sender as? UpperViewController{
            upperVC.upperPlayer?.play()
        }else{
            lowerVC.lowerPlayer?.play()
        }

    }
    
}
